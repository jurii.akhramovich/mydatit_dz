"use strict";

// ## Теоретический вопрос

/*
*
1. Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
*  Экранирование - это способ показать компилятору или интерпретатору, что мы хотим показать именно этот символ.
*
* */

function createNewUser(){
    const newUser = {
        getLogin(firstName, lastName){
            return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
        },
        getPassword(firstName, lastName, birthday){
            return this.firstName.toUpperCase()[0] + this.lastName.toLowerCase() + this.birthday.slice(6);
        },
        setFirstName(firstName){
            this.firstName = firstName;
        },
        getFirstName(){
            return this.firstName;
        },
        setLastName(lastName){
            this.lastName = lastName;
        },
        getLastName(){
            return this.lastName;
        },
        getAge(birthday){
            return new Date().getFullYear() - this.birthday.slice(6);
        },
    };
    newUser.setFirstName(prompt("Enter your first name"));
    newUser.setLastName(prompt("Enter your last name"));
    newUser.birthday = prompt("Enter your birthday", `dd.mm.yyyy`);
    return newUser;
}

const user = createNewUser();
// console.log(user.getLogin());
console.log(user.getPassword());
console.log(user.getAge());