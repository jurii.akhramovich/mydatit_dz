"use strict";

// ## Теоретический вопрос

/*
*
* 1. Опишите своими словами как работает цикл forEach.
*  Это цикл для массивов - он перебирает все элементы массива (проходит по всем).
*
* */

function filterBy(arr, type){
    let newArr = [];
    for (const element of arr) {
        if(typeof element !== type){
            newArr.push(element);
        }
    }
    return newArr;
};

console.log(filterBy(['hello', 6, undefined, ['string', 'number'], 'world', 23, '23', null], 'string'));