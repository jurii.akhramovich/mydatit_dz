"use strict";

//## Теоретический вопрос
/*
* Циклы используют для выполнения однотипных, повторяющихся действий, чтобы не дублировать код.
*/
let div = false;
let a;

do{
    a = +prompt("Enter your number");

    if(Number.isInteger(a)){
        for(let i = 0; i <= a; i++){
            if(i !== 0 && i % 5 === 0){
                div = true;
                console.log(i);
            }
        }
        if(!div){
            console.log("Sorry, no numbers");
        }
    }
}while(!Number.isInteger(a));