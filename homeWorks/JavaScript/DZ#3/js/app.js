"use strict";

// ## Теоретический вопрос

/*
*
* 1. Описать своими словами для чего вообще нужны функции в программировании.
* В первую очередь для того, чтобы не дублировать код. Этот код можно использовать много раз в разных местах программы.
* Также они нужны для проведения разных манипуляций и вычислений над даными.
*
* 2. Описать своими словами, зачем в функцию передавать аргумент.
* Для того, чтобы функция была "живой" и понимала, что именно ей нужно обработать.

* */

let num1 = "";
let num2 = "";
let operation = "";
let flag = true;

do{
    num1 = +prompt("Enter number 1", num1);
    num2 = +prompt("Enter number 2", num2);
    operation = prompt("Enter operation", operation);

    function isANumber(number){
        return (!isNaN(+number) && number !== null && number !== "");
    }

    function makeOperation(number1, number2, operation){
        switch (operation){
            case "+":
                return number1 + number2;
            case "-":
                return number1 - number2;
            case "/":
                return number1 / number2;
            case "*":
                return number1 * number2;
            default:
                break;
        }
    }

    let condition = isANumber(num1)&&isANumber(num2);
    let operations = operation === "+" || operation === "-" || operation === "/" || operation === "*";

    if(condition && operations){
        console.log(makeOperation(num1, num2, operation));
        flag = false;
    }

}while(flag);
