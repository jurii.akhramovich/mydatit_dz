"use strict";

// ## Теоретический вопрос

/*
*
* 1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.
* setTimeout() устанавливает единоразовую задержку, а setInterval() - устанавливает интервал времени, через который будет повтор выполнения кода
*
* 2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?
* * Если передать нулевую задержку, то код выполнится после выполнения всего остального кода
*
* 3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?
* Для того, чтобы не засорялась память.
*/

const img = Array.from(document.getElementsByClassName("image-to-show"));
const btnStop = document.querySelector("#btnStop");
const btnStart = document.querySelector("#btnStart");
let count = 0;
let pause = false;

img[count].classList.toggle("image-slide");
// fadeOut(".image-to-show");
count++;

setInterval(()=>{
    if(pause === false){
        showImg();
    }
}, 10000);

function showImg(){

    // fadeIn(".image-slide");
    img[count].classList.toggle("image-slide");
    // fadeOut(".image-to-show");

    if(count === 0){
        if(img[img.length-1].classList.contains("image-slide")){
            // fadeIn(".image-slide");
            img[img.length-1].classList.remove("image-slide");
            // fadeOut(".image-to-show");
        }
    }else {

        // fadeIn(".image-slide");
        img[count].previousElementSibling.classList.remove("image-slide");
        // fadeOut(".image-to-show");
    }
    console.log(count);
    count++;

    if(count === img.length) {
        count = 0;
    }
}

btnStop.addEventListener("click", ()=>{
    pause = true;
});

btnStart.addEventListener("click", ()=>{
    pause = false;
});

// function fadeOut(el) {
//     let opacity = 1;
//     let timer = setInterval(function() {
//         if(opacity <= 0.1) {
//             clearInterval(timer);
//             document.querySelector(el).style.display = "none";
//         }
//         document.querySelector(el).style.opacity = opacity;
//         opacity -= opacity * 0.1;
//     }, 50);
// }
//
// function fadeIn(el) {
//     let opacity = 0.01;
//     document.querySelector(el).style.display = "block";
//     let timer = setInterval(function() {
//         if(opacity >= 1) {
//             clearInterval(timer);
//         }
//         document.querySelector(el).style.opacity = opacity;
//         opacity += opacity * 0.1;
//     }, 50);
// }
