"use strict";

// ## Теоретический вопрос

/**
 * #1
 * var - бластью видимости переменной, объявленной директивой var, является вся функция, в которой она объявлена;
 * let - объявляет переменную с блочной областью видимости с возможностью инициализировать её значением;
 * const - объявляет именованную константу только для чтения.
 *
 * #2
 * Ициализация с помощью var чревато разного рода ошибками, поэтому этот способ не рекомендуется.
 */

let condition;
let name = "";
let age = "";

do {

    name = prompt("Enter your name", name);
    age = prompt("Enter your age", age);

    condition = ((!isNaN(+age) && age !== null && age !== "" && age !== undefined)&&
                (name !== null && name !== "" && name !== undefined));
    if(condition){
        if(age < 18){
            alert(`You are not allowed to visit this website`);
        }else if(age >= 18 && age <= 22){
            let answer = confirm(`Are you sure you want to continue?`);
            answer ? alert(`Welcome, ${name}`) : alert(`You are not allowed to visit this website`);
        }else{
            alert(`Welcome, ${name}`)
        }
    }
}while(!condition)
