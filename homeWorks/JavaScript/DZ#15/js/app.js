"use strict";

// ## Теоретический вопрос

/*
*
* 1. Напишите как вы понимаете рекурсию. Для чего она используется на практике?
* Рекурсия – это когда функция вызывает сама себя. Её используют для упрощения кодя, рекурсивное решение задачи обычно короче, чем итеративное.
*
* */
function isNum(number){
    return (!isNaN(+number) && number !== null && number !== "");
}
function getFactorial(n){
    let result = 1;
    while(!isNum(n)) {
            n = prompt("Enter your number", n);
    }
    for (let i = 1; i <= n; i++) {
        result *= i;
    }
    alert(result);
}

let n = getFactorial(prompt("Enter your number"));