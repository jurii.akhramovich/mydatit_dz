`use strict`

let ourWorkAllImgs = Array.from(document.querySelectorAll(".our-work__img"));
const ourWorkAll = document.querySelector("#our-work_all");
const ourWorkGraphicDesign = document.querySelector("#our-work_graphic-design");
const ourWorkWebDesign = document.querySelector("#our-work_web-design");
const ourWorkLandingPages = document.querySelector("#our-work_landing-pages");
const ourWorkWordpress = document.querySelector("#our-work_wordpress");
const ourWorkImgContainer = document.querySelector("#our-work__img-container");

const ourWorkBtn = document.querySelector("#our-work__btn");
const galleryBtn = document.querySelector("#gallery__btn");

const ourWorkLoad = document.querySelector("#our-works__load");
const galleryLoad = document.querySelector("#gallery__load");

let ourWorkMasImg = ["./img/our-works/img_4-1.jpg", "./img/our-works/img_4-2.jpg", "./img/our-works/img_4-3.jpg", "./img/our-works/img_4-4.jpg",
                     "./img/our-works/img_5-1.jpg", "./img/our-works/img_5-2.jpg", "./img/our-works/img_5-3.jpg", "./img/our-works/img_5-4.jpg",
                     "./img/our-works/img_6-1.jpg", "./img/our-works/img_6-2.jpg", "./img/our-works/img_6-3.jpg", "./img/our-works/img_6-4.jpg",
                     "./img/our-works/img_7-1.jpg", "./img/our-works/img_7-2.jpg", "./img/our-works/img_7-3.jpg", "./img/our-works/img_7-4.jpg",
                     "./img/our-works/img_8-1.jpg", "./img/our-works/img_8-2.jpg", "./img/our-works/img_8-3.jpg", "./img/our-works/img_8-4.jpg",
                     "./img/our-works/img_9-1.jpg", "./img/our-works/img_9-2.jpg", "./img/our-works/img_9-3.jpg", "./img/our-works/img_9-4.jpg"];
let ourWorkMasClass = ["wordpress", "graphic-design", "web-design", "landing-pages",
                       "web-design", "landing-pages", "wordpress", "graphic-design",
                       "landing-pages", "wordpress", "web-design", "graphic-design",
                       "web-design", "landing-pages", "graphic-design", "wordpress",
                       "web-design", "wordpress", "landing-pages", "graphic-design",
                       "landing-pages", "web-design", "graphic-design", "wordpress"];
let ourWorkCount = 1;
let ourWorkShowImg = 0;

ourWorkAll.addEventListener("click", (evt)=>{
    evt.preventDefault();
    visibleElem();
})

ourWorkGraphicDesign.addEventListener("click", (evt)=>{
    visibleElem();
    hiddenElem("graphic-design", evt);
})

ourWorkWebDesign.addEventListener("click", (evt)=>{
    visibleElem();
    hiddenElem("web-design", evt);
})

ourWorkLandingPages.addEventListener("click", (evt)=>{
    visibleElem();
    hiddenElem("landing-pages", evt);
})

ourWorkWordpress.addEventListener("click", (evt)=>{

    visibleElem();
    hiddenElem("wordpress", evt);
})

ourWorkBtn.addEventListener("click", ()=>{
    ourWorkLoad.classList.remove("hidden");
    setTimeout(()=>{
        ourWorkLoad.classList.toggle("hidden");
        showImg();
        if(ourWorkCount === 2){
        ourWorkBtn.classList.toggle("hidden");
    }
    ourWorkCount++;
    }, 2000);

})

galleryBtn.addEventListener("click", ()=>{
    galleryLoad.classList.remove("hidden");
    setTimeout(()=>{
        galleryLoad.classList.toggle("hidden");
        showGalleryImg();
        galleryBtn.classList.toggle("hidden");
    }, 2000);

})

$(function() {
    let $slider_num = 1;
  $(".client-feedback__img").click(function(e) {
      e.preventDefault();
      if($(this).attr('data') !== $slider_num) {
          $slider_num = $(this).attr('data');
          changeSlider($slider_num);
      }
  });
    $(".arrow").click(function(e) {
        e.preventDefault();
        $idBlock = $(this).attr('id');
        if($idBlock==='arrow_prev') {
            $slider_num -= 1;
        } else {
            $slider_num += 1;
        }
        if($slider_num > $('.client-feedback__container').length) {
            $slider_num = 1;
        }
        if($slider_num < 1) {
            $slider_num = $('.client-feedback__container').length;
        }
        changeSlider($slider_num);
    });
    function changeSlider($num) {
        $('.client-feedback__container').hide();
        $('#client-feedback__'+$num).slideDown('fast').css('display', 'flex');
    }
});


function hiddenElem (name, evt) {
    evt.preventDefault();
    ourWorkAllImgs.forEach((item) => {
        if(!item.classList.contains(name)) {
            item.classList.toggle("hidden");
        }
    })
}

function visibleElem(){
    ourWorkAllImgs.forEach((item) => {
        item.classList.remove("hidden");
    })
}

function showImg(){
    for(let i = ourWorkShowImg; i < (ourWorkShowImg + 12); i++){
        const newImg = document.createElement("img");
        newImg.setAttribute("src", ourWorkMasImg[i]);
        newImg.classList.toggle("our-work__img");
        newImg.classList.toggle(ourWorkMasClass[i]);
        ourWorkImgContainer.append(newImg);
    }
    ourWorkAllImgs = Array.from(document.querySelectorAll(".our-work__img"));
    ourWorkShowImg += 12;
}

function showGalleryImg(){
    console.log("Done!")
}