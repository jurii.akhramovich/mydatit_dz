`use strict`;

// Task #1

// for(let i = 0; i <= 300; i++){
//     if(i%2 == 1) {
//         if (i % 5 != 0) {
//             console.log(i);
//         }
//     }
// }

// Task #2

// let num1 = null;
// let num2 = null;
//
// do{
//     num1 = prompt("Enter number 1");
//     num2 = prompt("Enter number 2");
// }while((isNaN(+num1) || num1 === null || num1 === "" || num1 === undefined)||
//        (isNaN(+num2) || num2 === null || num2 === "" || num2 === undefined));
//
// console.log(`You enter ${num1} and ${num2}`);

// ИЛИ

// let condition;
//
// do{
//     let num1 = prompt("Enter number 1");
//     let num2 = prompt("Enter number 2");
//
//     condition = (!isNaN(+num1) && num1 !== null && num1 !== "" && num1 !== undefined)&&
//                 (!isNaN(+num2) && num2 !== null && num2 !== "" && num2 !== undefined);
//     if(condition){
//         console.log(`You enter ${num1} and ${num2}`);
//     }
// }while(!condition);

// Task #3

// let name;
// let lastName;
// let year;
//
// do{
//     name = prompt("Enter your name");
//     lastName = prompt("Enter your lastname");
//     year = +prompt("Enter your birthday");
//
//     if(name !== "" && lastName !== "" && year > 1910 && year < 2020){
//         console.log(`Your name is ${name}, lastName ${lastName} and your birthday is ${year}`);
//         break;
//     }
// }while(true);

// Task #4

let num1;
let num2;
let operation;
let operations;
let flag = true;

while(flag){
    num1 = +prompt("Enter number 1");
    num2 = +prompt("Enter number 2");
    operation = prompt("Enter operation", "+, -, /, *, **");

    condition = (!isNaN(+num1) && num1 !== null && num1 !== "")&&
                (!isNaN(+num2) && num2 !== null && num2 !== "");
    operations = operation === "+" || operation === "-" || operation === "/" || operation === "*" || operation === "**";

    if(condition && operations){
        switch (operation){
            case "+":
                console.log(`Над числами ${num1} и ${num2} была проведена операция ${operation}. Результат: ${+num1 + +num2}`);
                break;
            case "-":
                console.log(`Над числами ${num1} и ${num2} была проведена операция ${operation}. Результат: ${num1 - num2}`);
                break;
            case "/":
                console.log(`Над числами ${num1} и ${num2} была проведена операция ${operation}. Результат: ${num1 / num2}`);
                break;
            case "*":
                console.log(`Над числами ${num1} и ${num2} была проведена операция ${operation}. Результат: ${num1 * num2}`);
                break;
            case "**":
                console.log(`Над числами ${num1} и ${num2} была проведена операция ${operation}. Результат: ${num1 ** num2}`);
                break;
            default:
                break;
        }


        let finish = prompt("Continious?", "yes, no");
        if(finish === "no"){
            flag = false;
        }
    }

}


