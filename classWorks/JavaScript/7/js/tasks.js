"use strict";

// Task #1

// function repeat(subString, count) {
//     let newString = "";
//     // Proverka na stroky
//     if (typeof subString !== "string") {
//         return "ERROR";
//     }
//     // Proverka na 4islo
//     if(isNaN(+count) || typeof count !== "number"){
//         return "ERROR";
//     }
//     for (let i = 0; i < count; i++) {
//         newString += subString;
//     }
//     return newString;
// }
//
// console.log(repeat("Text", 3));

// Task #2

// function capitalizeAndDoublify(someString){
//     let newString = "";
//     for (const iterator of someString) {
//         newString += iterator.toUpperCase().repeat(2);
//     }
//     return newString;
// }
//
// console.log(capitalizeAndDoublify("text"));

// Task #3

const days = {
    0: 'Воскресенье',
    1: 'Понедельник',
    2: 'Вторник',
    3: 'Среда',
    4: 'Thursday',
    5: 'Friday',
    6: 'Saturday',
};

function getNameOfDay(daysAgo, daysObj){
    return daysObj[new Date(Date.now() - daysAgo * 24 * 60 * 60 * 1000).getDay()];
}

console.log(getNameOfDay(65, days));