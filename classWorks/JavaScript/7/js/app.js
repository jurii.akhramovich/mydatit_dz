"use strict";

// "text"; // `text`  'text'
//
// console.log(typeof typeof "text");
//
// const str = "Some \u00A9 text";
//
//
// console.log(str.length);
// console.log(str);
// console.log(str[0], str[5]);

// for (const iterator of str) {
//     console.log(iterator);
// }

// const str2 = str.toLowerCase();
// console.log(str2);
//
// const str3 = (str + " ").repeat(4);
// console.log(str3);
//
// console.log(str.includes("So", 3));
// console.log(str.indexOf("So", 3));
// console.log(str.lastIndexOf("t", str.length-1));
//
// console.log(str.startsWith(""));
// console.log(str.startsWith("S"));
//
// console.log(str.endsWith(""));
// console.log(str.endsWith("t"));
//
// const str5 = str.slice(3, 8);
// console.log(str5);
//
// const str6 = str.substring(3, 8);
// console.log(str6);
//
// const str7 = str.substr(3, 7); /// Выводится
// console.log(str7);
//
// console.log("Text" > "text");
// console.log("Text".charCodeAt(0));
// console.log("Text".codePointAt(3));
//
// console.log(String.fromCodePoint(65));


// DATE

const nowDate = new Date();
const date0 = new Date(0);

console.log(nowDate - date0);

console.log(new Date(1588553122593));

// console.log(nowDate);
// console.log(Date.now());
//
// console.log(nowDate.getDate());
// console.log(nowDate.getDay());
// console.log(nowDate.getUTCDay());
//
//
// nowDate.setMonth(10);
// console.log(nowDate);

