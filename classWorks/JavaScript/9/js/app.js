"use strict";

// let arr = [1, 2, "3", {name: "Vasya"}, [2, 3], null, false]; // new Array();
//
// console.log(arr);
// console.log(typeof arr);
//
// console.log(arr[3]);
//
// //----------------------------------------

// const arr = [];
//
// arr.push(3); // dobavlyaet element v konec
// arr.unshift(2); // dobavlyaet element v nachalo
// arr.push(4);
//
// // arr.pop(); // ydalyaet posledniy element
// // arr.shift(); // ydalyaet perviy element
//
// console.log(arr);

// for (let i = 0; i < arr.length; i++){
//     console.log(arr[i];
// }

// const arrItemsIncrement = (element, index, array) => {
//         array[index] = ++element;
// }
// arr.forEach(arrItemsIncrement);

// Str 27 - 30 toje samoe, 4to i str 33 - 36.

// peredaet callBackFunction
// arr.forEach((value, index, array) => {
//     array[index] = ++value;
// });
//
// arr.forEach((x, i, a) => {
//     a[i] = ++x;
//     console.log(++x);
// });
//
// console.log(arr);

//-----------------------------

//// SPLICE
// const newArr = arr.slice(1); // kopirovanie vubranuh elementov
// console.log(arr);
// console.log(newArr);

//// SPLICE
// arr.splice(1, 1, "33", false, null); // ydalyaet i pri neobhodimosti dobavlyaet
// arr.splice(1, 1, ...arr);
// console.log(arr);

// // CONCAT
// const newArr = arr.concat(arr);
// console.log(newArr);

// // FIND
// const arr2 = [{id: 1}, {id: 3}, {id: 4}, {id: 34}];
// let result = arr2.find(function(item, index, array){
//     return item.id > 4;
// });
// console.log(result);

// // FILTER
// const arr2 = [{id: 1}, {id: 3}, {id: 4}, {id: 34}];
// let results = arr2.filter((item) => item.id < 20);
// console.log(results);

// // MAP
// const arr2 = [{id: 1}, {id: 3}, {id: 4}, {id: 34}];
// console.log(arr2);
// let results = arr2.map((el) => el.id);
// console.log(results);

// // SORT
// const arr = [3, 2, 4];
// const arr2 = [{id: 1}, {id: 3}, {id: 4}, {id: 34}];
//
// arr.sort(compare);
// // arr2.sort((a, b) => a.id < b.id); // ili tak:
// arr2.sort((a, b) => {
//     // if(a > b) return 1;
//     // if(a === b) return 0;
//     // if(a < b) return -1; // ili tak:
//     return a.id - b.id;
// });
//
// function compare(a, b){
//     if(a > b) return 1;
//     if(a === b) return 0;
//     if(a < b) return -1;
// }
//
// console.log(arr);
// console.log(arr2);

// // SPLIT & JOIN
// let names = "Вася, Петя, Маша";
// let result = names.split(", ").reverse().join("->");
// console.log(result);

// // REDUCE
// const arr2 = [{id: 1}, {id: 3}, {id: 4}, {id: 34}];
// let result = arr2.reduce((summ, value) => (summ + value.id), 0); // 0 - eto startovoe zna4enie summ
// console.log(result);

// // thisArg
// let army = {
//     minAge: 18,
//     maxAge: 27,
//     canJoin(user) {
//         return user.age >= this.minAge && user.age < this.maxAge;
//     }
// };
//
// let users = [
//     {age: 16},
//     {age: 20},
//     {age: 23},
//     {age: 30}
// ];
//
// // найти пользователей, для которых army.canJoin возвращает true
// let soldiers = users.filter(army.canJoin, army);
//
// console.log(soldiers.length);
// console.log(soldiers);