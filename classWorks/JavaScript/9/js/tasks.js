"use strict";

// Task #1
//
// let storage = [
//     'apple',
//     'water',
//     'banana',
//     'pineapple',
//     'tea',
//     'cheese',
//     'coffee',
// ];
//
// const replaceItems = (name, list) => {
//     let index= storage.findIndex(function(item, i, array){
//         return item === name;
//     })
//     if(index=== -1){
//         return "Error. This product does not exist.";
//     }
//     if(!Array.isArray(list)){
//         return "Error. This list does not exist.";
//     }
//     storage.splice(index, 1, ...list);
// }
// console.log(replaceItems("tea", ["book", "toy"]));
// console.log(storage);

// Task 3

const days = {
    ua: [
        'Понеділок',
        'Вівторок',
        'Середа',
        'Четвер',
        'П’ятниця',
        'Субота',
        'Неділя',
    ],
    ru: [
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота',
        'Воскресенье',
    ],
    en: [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday',
    ],
};

function organizer(){
    const langs = Object.keys(days);
    let condition;
    let lang;
    do{
        lang = prompt("Choose language", "ua, ru, en");
        condition = langs.find((item) => item === lang);

    }while(!condition);

    days[lang].forEach((el) => {
        console.log(el);
    });
}

organizer();