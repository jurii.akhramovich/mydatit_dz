"use strict";

// -------- TASK #1 --------------------------------

const form = document.querySelector("#form");
const reminder = document.querySelector("#reminder");
const seconds = document.querySelector("#seconds");
const btn = document.querySelector("#btn");

form.addEventListener("submit", (evt)=>{
    evt.preventDefault();
    if(reminder.value.trim() === ""){
        alert("Ведите текст напоминания.");
        return;
    }
    if(seconds.value <= 1){
        alert("Время задержки должно быть больше одной секунды.");
        return;
    }

    btn.setAttribute("disabled", "true");

    setTimeout(()=>{
        alert(reminder.value);
        form.reset();
        reminder.focus();
        btn.removeAttribute("disabled");
    }, seconds.value*1000);


});

// -------- TASK #2 --------------------------------

