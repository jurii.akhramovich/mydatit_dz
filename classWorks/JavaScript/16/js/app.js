"use strict";

// function logging(){
//     for(let index = 0; index < 10000; index++){
//         console.log("for");
//     }
//     setTimeout(() => {
//         alert(timer);
//     }, 500);
// }
//
// const timer = setTimeout(logging, 1000);
// // setInterval(logging, 1000);

// const name = "Roman";
//
// sessionStorage.setItem("name", name);
// localStorage.setItem("name", name);
// console.log(sessionStorage);
// console.log(localStorage);
//
// const name2 = localStorage.getItem("name");
// console.log(name2);
//
// console.log(localStorage.key(0));
//
// console.log(localStorage);

const user = {
    name: "Roman",
    age: 18,
};

const arr = [1, 2, 3, 5, 6];

let temp;

// temp = JSON.stringify(user);
// console.log(temp);
// console.log(typeof temp);
//
// sessionStorage.setItem("user", temp);
//
// temp = sessionStorage.getItem("user");
// console.log(temp);
// console.log(typeof temp);
//
// const userFromTemp = JSON.parse(temp);
// console.log(userFromTemp);
// console.log(typeof userFromTemp);

sessionStorage.setItem("arr", arr);
sessionStorage.setItem("arr", JSON.stringify(arr));
sessionStorage.getItem("arr");

console.log(sessionStorage);
