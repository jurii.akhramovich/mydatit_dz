"use strict";

const link = document.querySelector(".link");
const wrapper = document.querySelector("#wrapper");

// link.addEventListener("click", (evt)=>{
//     evt.preventDefault();
//     console.log("Click on link");
//     console.log(evt);
// })

wrapper.addEventListener("click", (evt)=>{
    evt.preventDefault();
    console.log("Click on wrapper");
    console.log(evt);
    console.log(evt.target);
}, true)

document.body.addEventListener("click", (evt)=>{
    console.log("Click on body");
    evt.stopPropagation();
    // console.log(evt);
}, false)
