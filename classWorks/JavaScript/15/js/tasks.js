"use strict";

// // -------- TASK #1 --------------------------------
//
// const menu = document.querySelector("#menu");
//
// menu.addEventListener("click", (evt)=>{
//     evt.preventDefault();
//     console.log(evt.target);
//     const li = menu.getElementsByTagName("li");
//     Array.from(li).forEach((item)=>{
//         item.classList.remove("active");
//     })
//     console.log(li);
//     evt.target.closest("li").classList.add("active");
// })

// -------- TASK #2 --------------------------------

const messages = document.querySelector("#messages");

messages.addEventListener("click", (evt)=>{
    const li = messages.getElementsByTagName("li");
    if(evt.target.closest(".close-btn")) {
        evt.target.closest(".pane").setAttribute("hidden", "");
    }
    console.log(evt.target);
})