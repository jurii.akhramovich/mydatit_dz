"use strict";

// // Task #1
//
// let btn = document.createElement('button');
// let root = document.getElementById("root");
//
// btn.textContent = "Войти";
// btn.addEventListener("click", () => {
//     alert("Добро пожаловать!");
// })
//
// // root.append(btn);
//
// // Task #2
//
// const clickHandler = function(){
//     alert("При клике по кнопке вы войдёте в систему.");
//     btn.removeEventListener("mouseover", clickHandler);
// }
//
// btn.addEventListener("mouseover", clickHandler);
// root.append(btn);

// Task #3

const PHRASE = 'Добро пожаловать!';

function getRandomColor() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);

    return `rgb(${r}, ${g}, ${b})`;
}

const title = document.createElement("h1");
title.textContent = PHRASE;

let root = document.getElementById("root");
root.append(title);

const btn = document.createElement("button");
btn.textContent = "Раскрасить";
title.after(btn);

btn.addEventListener("click", function (){
    let arr = PHRASE.split("");
    let newArr = arr.map(item => {
        let span = document.createElement("span");
        span.textContent = item;
        span.style.color = getRandomColor();
        return span;
    });
    const fragment = document.createDocumentFragment();
    // newArr.forEach((el) => {
    //     fragment.append(el)
    // })
    fragment.append(...newArr);

    title.innerHTML = "";
    title.append(fragment);
})

