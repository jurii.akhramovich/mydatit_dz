"use strict";


// window.addEventListener("DOMContentLoaded", (event) => {
//     const clickHandler = () => {
//         alert("click");
//     }
//
//     const onClickBtn = () => {
//         alert("click 2");
//     }
//
//     const btn = document.getElementById("click-me");
//
// // btn.onclick = clickHandler;
// // btn.onclick = onClickBtn;
//
// // btn.addEventListener('click', clickHandler);
// // btn.addEventListener('click', onClickBtn);
//
// // (16-17) ili tak: (21-24)
//
//     btn.addEventListener('click', function(evt){
//         console.log(evt);
//         onClickBtn();
//         clickHandler();
//
//         console.log(this);
//         console.log(evt.target);
//     });
//
// })


const root = document.getElementById("root");

const fragmentHTML = document.createDocumentFragment();
const clickHandler = (evt) => {
    const element = evt.target;
    // element.style.backgroundColor = "yellow";
    alert(`li clicked ${element.textContent}`);
    element.removeEventListener("click", clickHandler);
};

for(let i = 0; i < 10; i++){
    const li = document.createElement("li");
    li.textContent = i;
    li.addEventListener("click", clickHandler)

    fragmentHTML.append(li);
}

const list = document.createElement("ul");
root.prepend(list);


list.append(fragmentHTML);