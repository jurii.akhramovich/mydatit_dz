"use strict";

// const user = {
//     name: "Vasya",
//     "last name": "Pupkin",
//     age: 18,
//     getFullName: function(){
//         return `${this.name} ${this["last name"]}`;
//     },
// };
//
// // user.name = "Vasya";
//
// console.log(user.getFullName());
// console.log(user["last name"]);

// ----------------------------------------

const user = {
    set name(value){
        if(value.length < 2){
            return "Error";
        }else{
            this._name = value;
        }
    },
    get name(){
        return this._name;
    },
};

user.name = prompt("Print your name");
console.log(user.name);

// ----------------------------------------

// const user = {
//     set name(value){
//         if(value.length < 2){
//             return "Error";
//         }else{
//             this._name = value;
//         }
//     },
//     get name(){
//         return this._name;
//     },
//     gender: "",
//     setGender: function(){
//         // this.gender = prompt("gender");
//         this.gender = "male";
//     },
//     getGender(){
//         console.log(this.gender);
//     },
//     family:{
//         closest:{
//             children: 3,
//             parents: 2,
//         },
//     },
// };
// // user.setGender();
// // user.getGender();
//
// // ----------------------------------------
//
// // for (const key in user) {
// //     // console.log(key);
// //     console.log(`${key} - ${user[key]}`);
// // }
//
// // ----------------------------------------
//
// user.name = "Vasya";
//
// // const user2 = user;
// const user2 = Object.assign({}, user);
//
// user2.name = "Petya";
//
// console.log(user.name);
// console.log(user2.name);
//
// // ----------------------------------------
//
// function UserCreator(name, lastName, age){
//     this.name = name;
//     this.lastName = lastName;
//     this.age = age;
//     this.getAge = function (){
//         console.log(this.age);
//     };
// }
//
// const user3 = new UserCreator("Vovan", "Pchelkin", 33);
//
// console.log(user3);