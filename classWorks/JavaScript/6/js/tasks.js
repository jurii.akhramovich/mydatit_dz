"use strict";

// Task #1

const user = {
    name: "Ivan",
    "last name": "Ivanov",
    prof: "programmer",
    sayHi(){
        console.log(`Hi, my name is ${this.name} ${this["last name"]}`);
    },
    propertyValueChange(propertyName, propertyValue){
        if(this.hasOwnProperty(propertyName)){
            this[propertyName] = propertyValue;
        }else{
            console.log("Property is not exist");
        }
    },
};
user.sayHi();
user.propertyValueChange("name", "Raisa");
user.sayHi();

