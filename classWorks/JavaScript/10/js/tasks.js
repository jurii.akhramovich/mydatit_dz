"use strict";

// Task #1

// const list = document.getElementById("list");
// console.log(list.innerHTML);
// console.log(list.innerText);
// console.log(list.outerHTML);
//
// const item = document.getElementsByClassName("list-item");
// console.log(item[0].innerHTML);
// console.log(item[1].innerText);
// console.log(item[2].outerHTML);
//
// const tags = document.getElementsByTagName("li");
// console.log(tags[0].innerHTML);
// console.log(tags[1].innerText);
// console.log(tags[2].outerHTML);
//
// const li = document.querySelector("li:nth-child(3)");
// console.log(li.innerHTML);
// console.log(li.innerText);
// console.log(li.outerHTML);
//
// const lis = document.querySelectorAll("li");
// console.log(lis[2].innerHTML);
// console.log(lis[3].innerText);
// console.log(lis[4].outerHTML);

// Task #2

// const btnEl = document.querySelector(".remove");
// console.log(btnEl);
//
// btnEl.remove();
// document.body.prepend(btnEl);
//
// btnEl.classList.remove("btn");
// btnEl.classList.add("btn-2");
// // vmesto 38 i 39 -> 41
// btnEl.className = "remove btn-2";
//
// btnEl.classList.toggle("remove");
// btnEl.classList.replace("btn-2", "btn");

// Task #3

const storeEls = document.querySelectorAll(".store li");

for(let i = 0; i < storeEls.length; i++){
    let last = +storeEls[i].textContent.split(": ")[1];
    if(last === 0){
        storeEls[i].style.color = "red";
        storeEls[i].style.fontWeight = "600";
        storeEls[i].textContent = storeEls[i].textContent.replace("0", "закончился");
    }
};