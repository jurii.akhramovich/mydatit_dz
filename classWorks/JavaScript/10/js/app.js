"use strict";

// console.log(window);
//
// console.log(document);
//
// const bodyStyle = document.body.style;
// document.body.style.backgroundColor = "yellow";
//
// console.log(bodyStyle);
//
// document.body.children[0].style.color = "darkblue";
// document.body.childNodes[0].textContent = "first text node";
//
// const nodes = document.body.childNodes;
// const children = document.body.children;
//
// nodes.forEach((el, index) =>{
//     console.log(`${index} => ${el}`);
// });
// // for (const index in children) {
// //     console.log(`${index} => ${children[index]}`);
// // }
// //
// // console.log(children.length);
// Array.from(children).forEach((el, index) =>{
//     console.log(`${index} => ${el}`);
// });

//------------------------------------------------------

// const  paragraf = document.getElementsByTagName("p");
// console.log(paragraf);
//
// const  paragraf2 = document.getElementsByClassName("text-block");
// console.log(paragraf2);

const  paragraf3 = document.getElementById("text2");
console.log(paragraf3);
// console.log(document.body.children.text);
//
// const  paragraf4 = document.querySelector(".text-block");
// console.log(paragraf4);
//
// const  paragraf5 = document.querySelectorAll(".text-block");
// console.log(paragraf5);

paragraf3.style.cssText = "font-size: 28px; color: #000;";

paragraf3.innerText = "<span>text</span>";
paragraf3.innerHTML += "<span>text2</span>";
paragraf3.insertAdjacentText("afterbegin","<span>text3</span>");
paragraf3.insertAdjacentHTML("afterbegin","<span>text4</span>");
paragraf3.textContent = "<span>text5</span>";

const strongElement = document.createElement("strong");
strongElement.textContent = "created by JS"
paragraf3.append("<strong>tag 1</strong>");
paragraf3.prepend(strongElement);
paragraf3.after(strongElement);
paragraf3.before("<strong>tag 4</strong>");

