"use strict";

// Task #1

// function summ(a, b){
//     let result = a + b;
//
//     return result;
// }
//
// console.log(summ(23, 56));

// Task #2
//
// function argsCounter(){
//     return arguments.length;
// }
//
// argsCounter();
// console.log(argsCounter());

// Task #3
//
// function count(numFirst, numLast){
//     if(numFirst > numLast){
//         console.log("Error!!!");
//     }else if(numFirst === numLast){
//         console.log("Nothing to count!");
//     }else{
//         console.log("Count begin!");
//         for(let i = numFirst; i <= numLast; i++){
//             console.log(i);
//         }
//         console.log("Finish!");
//     }
// }
//
// count(3, 3);

// Task #4
//
// function count(numFirst, numLast, numDiv){
//     if(arguments.length !== 3){
//         console.log("ERROR");
//         return;
//     }
//     if((numFirst !== NaN && numFirst !== "" && !isNaN(numFirst) ||
//         (numLast !== NaN && numLast !== "" && !isNaN(numLast) ||
//         (numDiv !== NaN && numDiv !== "" && !isNaN(numDivt){
//
//         console.log("It's Not a Number!!!")
//         return;
//     }
//     if(numFirst > numLast){
//         console.log("Error!!!");
//         return;
//     }else if(numFirst === numLast){
//         console.log("Nothing to count!");
//         return;
//     }
//
//     console.log("Count begin!");
//     for(let i = numFirst; i <= numLast; i++){
//         if(i%numDiv === 0) {
//             console.log(i);
//         }
//     }
//     console.log("Finish!");
// }
//
// count(2, 9, 3);
//

// Task #5

function summm(){
    if(arguments.length < 2){
        console.log("ERROR");
        return;
    }

    for (let i = 0; i < arguments.length; i++){
        if(arguments[i] === NaN || arguments[i] === "" || isNaN(arguments[i])){
            return;
        }
    }

    let result = 0;
    for (let i = 0; i < arguments.length; i++){
        result += arguments[i];
    }
    console.log(result);
}
summm(2, "h", 5);
