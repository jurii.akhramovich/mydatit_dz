"use strict";

// let a = 34;
//
// console.log(`window a: ${window.a}`);
// console.log(`a: ${a}`);
//
// console.log(window);
// console.log(this);

const  num1 = 3;
const  num2 = 5;

function summ(){
    console.log(num1 + num2);
    // console.log(arguments);
}

summ();

let result;
const sum = function(a, b){
    a = a ** 2;
    b = b ** 2;
    let result = a + b;

    return result;
};

result = sum(num1, num2);
console.log(result);

result = sum("2", "3", 3, 44);
console.log(result);