'use strict';

let str = "Some string";
let maxWidth = 3;

let result;

result = maxWidth ** 3;
console.log(result);

result = maxWidth++;
console.log("result: " + result);
console.log("maxWidth: " + maxWidth);

result = ++maxWidth;
console.log("result: " + result);
console.log("maxWidth: " + maxWidth);

console.log(`result: ${result}`); // shablonnye stroki - toje samoe, 4to i v stroke 16
console.log(`maxWidth: ${maxWidth}`);

const booleanVar = true;
const numberVar = 33;
const nullVar = null;
const undefVar = undefined;

// result = String(booleanVar);
result = toString(booleanVar); // preobrazovanie Object-ov
console.log(`result: ${result}`);
console.log(`typeof result: ${typeof result}`);
