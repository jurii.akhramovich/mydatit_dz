// console.log("It's work!!!!!!");

// var name="Roman";
//
// const age = prompt("How old are you?", "35"); // value or null
// // alert("Done!");
// // alert(name);
//
// console.log(age);
//
// const agree = confirm("are you agree?"); // true or false
// console.log(agree);

let groupAdminName = "Jurii";

let num = 3;
console.log(typeof num); // number

let str1 = "3";
console.log(typeof str1); // string

let empty = null;
console.log(typeof empty); // object

let undef = undefined;
console.log(typeof undef); // undefined

let agree = true;
console.log(typeof agree); // boolean

let result1 = 3 / "abv";
console.log(result1); // NaN
console.log(typeof result1); // NaN is number

// Array,  Function, Object

let num1 = 3;
let num2 = 5;
let str = "35";
let yes = true;

let result = null;
console.log(result);

result = 3 + 3;
console.log(result);

result = num1 + num2;
console.log(result);

result = num1 + str;
console.log(result);

result = num1 + +str;
console.log(result);

result = num2 + yes;
console.log(result);

result = num1 - num2;
console.log(result);

result = str - num2;
console.log(result);

result = num1 / num2;
console.log(result);

result = num1 / 0;
console.log(result);

result = num1 / str;
console.log(result);

result = str / num1;
console.log(result);

result = num1 / false;
console.log(result);


let num4 = "12.3";
console.log(typeof num4);

console.log(parseInt(num4));
console.log(parseFloat(num4));
console.log(+(num4));

let a = +prompt("Enter number 1");
let b = +prompt("Enter number 2");
console.log(a);
console.log(b);
console.log(a + b);
console.log(a - b);
console.log(a * b);
console.log(a / b);
console.log(a % b);