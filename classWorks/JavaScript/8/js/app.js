"use strict";

// let a = 2,
//     b = 4;
// let result = (a = 2, b = 4) =>{
//     return  a + b;
// };
// console.log(result(3, 5));
//
// //-------------------------------------
//
// const user = {
//     name: "Vasya",
//     getName: function() {
//         // debugger;
//         console.log(this.name);
//     },
// };
//
// // debugger;
//
// user.getName();

/*
* () => a // return a by default
* () => {return a}
* */

//-------------------------------------

//Task 2

// const getSumm = (...rest) => {
//     let sum = 0;
//     for(let i = 0; i < rest.length; i++) {
//         sum += rest[i];
//     }
//     return sum;
// }
// console.log(getSumm(3, 4, 5, 6));