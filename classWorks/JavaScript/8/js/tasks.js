"use strict";

// Task #1

// let getSum1 = (a = 2, b = 3) => a +b;
// let getSum2 = (a = 2, b = 3) => {
//     return a + b;
// };
//
// console.log(getSum1(3, 4));
// console.log(getSum2(4, 5));

// Task #3

// const getMax = (...rest) => {
//     if(rest.length < 2){
//         return "Error";
//     }
//     for(let i = 0; i < rest.length; i++){
//         if(!isNaN(rest[i]) || typeof rest[i] !== "number"){
//             return "Error in " + (i + 1) + " argument";
//         }
//     }
//     return Math.max(...rest); // Peredali kak spisok
// }
//
// console.log(getMax(12, "s", 9, 0, -1));

// Task #4

let log = (message = "Empty string", count = 1) => {
    for(let i = 0; i < count; i++){
        console.log(message);
    }
}

log ("Hello", 3);