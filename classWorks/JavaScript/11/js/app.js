"use strict";

const root = document.getElementById("root");

for (let index = 0; index < 10; index++) {
    const div = document.createElement("div");
    div.textContent = index;
    let color = `rgb(
        ${Math.floor(Math.random() * 255)},
        ${Math.floor(Math.random() * 255)},
        ${Math.floor(Math.random() * 255)})`;

    div.setAttribute("data-color", color);

    div.style.color = div.getAttribute("data-color");
    // div.style.color = color;
    div.style.fontWeight = "700";
    div.style.fontSize= "40px";

    document.body.prepend(div);
}

const divs = document.getElementsByTagName("div");

Array.from(divs).forEach((element) => {
    console.log(element.textContent);
});

Array.from(divs).forEach((element, index) => {
    element.className = `element_${index}`;
});

const fifthDiv = document.getElementsByClassName("element_4" )[0];

let className = fifthDiv.getAttribute("class");
console.log(className);

fifthDiv.setAttribute("id", "el-4");

fifthDiv.style.color = fifthDiv.getAttribute("data-color");

console.log(fifthDiv);
console.log(`fifthDiv.style.width: ${fifthDiv.style.width}`);
console.log(`fifthDiv.offsetWidth: ${fifthDiv.offsetWidth}`);

console.log(`document.body.clientHeight: ${document.body.clientHeight}`);
console.log(`document.body.clientWidth: ${document.body.clientWidth}`);

console.log(`fifthDiv.getBoundingClientRect().top: ${fifthDiv.getBoundingClientRect().top}`);

console.log(`document.body.scrollHeight: ${document.body.scrollHeight}`);
console.log(`document.body.scrollWidth: ${document.body.scrollWidth}`);

console.log(`fifthDiv.scrollTop: ${fifthDiv.scrollTop}`);
console.log(`documentBody.scrollTop: ${document.body.scrollTop}`);


console.log(`window.innerWidth: ${window.innerWidth}`);
console.log(`window.innerHeight: ${window.innerHeight}`);
console.log(`window.pageYOffset: ${window.pageYOffset}`);
console.log(`window.window.pageXOffset: ${window.pageXOffset}`);

// Array.from(divs).forEach((element, index) => {
//     if(index%2 === 0) {
//         element.remove();
//     }
// });
//
// // for(let i = 0; i < 5; i++){
// //     divs[i].remove();
// // }
//
// Array.from(divs).forEach((element) => {
//     console.log(element.textContent);
// });